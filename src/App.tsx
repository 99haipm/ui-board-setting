import React, { useState } from 'react';
import { styled } from 'styled-components';
import {  Button, Col, Row, Select, message} from 'antd';
import { DragDropContext, DragDropContextProps, Draggable, Droppable } from 'react-beautiful-dnd';
import FieldCardComponent, { FieldComponentProps } from './components/fieldComponents/FieldComponent';
import {LayoutSetting } from './components/config/MainPageModel';
import Layout2 from './components/config/Layout_2';
import Layout3 from './components/config/Layout_3';
import Layout1 from "./components/config/Layout_1";


interface DroppableRootProps{
  isDraggingOver: boolean;
}

const DroppableRoot = styled.div<DroppableRootProps>`
  height:100%;
  width:30vw;
  overflow-y: auto;
`



function App() {

  const constantFields: FieldComponentProps[] = [
    {
      id: "1",
      description: "desc",
      name: "Summary",
      type: "text",
      layoutPermission:{
        layout1:true,
        layout2:false,
        layout3:true,
        layout4:false
      }
    },
    {
      id: "2",
      description: "desc",
      name: "Description",
      type: "text",
      layoutPermission:{
        layout1:true,
        layout2:true,
        layout3:true,
        layout4:false
      }
    },
    {
      id: "3",
      description: "desc",
      name: "Asignee",
      type: "user-picker",
      layoutPermission:{
        layout1:true,
        layout2:false,
        layout3:false,
        layout4:false
      }
    },
    {
      id: "4",
      description: "Comment",
      name: "Comment",
      type: "Comment",
      layoutPermission:{
        layout1:false,
        layout2:false,
        layout3:false,
        layout4:false
      }
    }
  ]
  
  const [fieldItems, setFieldItems] = useState<FieldComponentProps[]>(constantFields)
  const [selectedLayout, setSelectedLayout] = useState<string>("layout-1");
  const [itemDragging, setItemDragging] = useState<FieldComponentProps>();
  const [layoutSetup, setLayoutSetUp] = useState<Record<string, number[]>>({
    "Vertical": [0,0],
    "Horizontal": [0,0]
  });
  const constantLayout: LayoutSetting[] = [
    {
      id: "layout-1",
      name:"layout 1",
      items: []
    },
    {
      id: "layout-2",
      name:"layout 2",
      items: []
    },
    {
      id: "layout-3",
      name:"layout 3",
      items: []
    },
    {
      id: "layout-4",
      name:"layout 4",
      items: []
    }
  ]
  const [layouts, setLayouts] = useState<LayoutSetting[]>(constantLayout)
 

  const handleDragEnd : DragDropContextProps["onDragEnd"] = ({source,destination}) => {
    
    console.log(source)
    console.log(destination)
    if(!source && ! destination){
      return;
    }
    if(source.droppableId == "pool"){
      switch(destination?.droppableId){
          case "layout-1":
            if(!itemDragging?.layoutPermission.layout1){
              setItemDragging(undefined)
              return;
            }
            break;
          case "layout-2":
            if(!itemDragging?.layoutPermission.layout2){
              setItemDragging(undefined)
              return;
            }
            break;
          case "layout-3":
            if(!itemDragging?.layoutPermission.layout3){
              setItemDragging(undefined)
              return;
            }
            break;
          case "layout-4":
            if(!itemDragging?.layoutPermission.layout4){
              setItemDragging(undefined)
              return;
            }
            break;
      }
    }
    if(source.droppableId !== destination?.droppableId){
      if(source.droppableId === "pool"){
        const cloneFieldItems:FieldComponentProps[]  = JSON.parse(JSON.stringify(fieldItems));
        const moveItemField = cloneFieldItems[source.index];
        const cloneLayoutState:LayoutSetting[]  = JSON.parse(JSON.stringify(layouts));
        cloneLayoutState.forEach(
          layout => {
            if(layout.id === destination?.droppableId){
              layout.items.push(moveItemField)
            }
          }
        )
        cloneFieldItems.splice(source.index, 1);
        setFieldItems(cloneFieldItems)
        setLayouts(cloneLayoutState)
      }
      else if(source.droppableId.startsWith("layout") && destination?.droppableId.startsWith("layout")){
        const cloneLayoutState:LayoutSetting[]  = JSON.parse(JSON.stringify(layouts));
        const layoutSource =  cloneLayoutState.find(layout => layout.id === source.droppableId);
        const layoutDestination = cloneLayoutState.find(layout => layout.id === destination?.droppableId);
        if(layoutSource && layoutDestination){
          const moveItemField = layoutSource.items[source.index];
          layoutSource.items.splice(source.index, 1);
          layoutDestination.items.push(moveItemField);
          setLayouts(cloneLayoutState);
        }
      }else{
        const cloneLayoutState:LayoutSetting[]  = JSON.parse(JSON.stringify(layouts));
        const layoutSource =  cloneLayoutState.find(layout => layout.id === source.droppableId);
        const cloneFieldItems:FieldComponentProps[]  = JSON.parse(JSON.stringify(fieldItems));
        if(layoutSource){
          const moveItemField = layoutSource.items[source.index];
          layoutSource.items.splice(source.index, 1);
          setLayouts(cloneLayoutState)
          cloneFieldItems.push(moveItemField)
          setFieldItems(cloneFieldItems)
        }
      }
    }
    setItemDragging(undefined)
  }

  const handleSelectLayoutType = (e: string) => {
    setSelectedLayout(e);
    setLayouts(constantLayout);
    setFieldItems(constantFields);
  }
  
  let renderLayout: React.ReactNode = <></>

  switch(selectedLayout){
    case "layout-1":
      renderLayout = <Layout1 layouts={layouts} setLayoutSetup={setLayoutSetUp} itemDragging={itemDragging}/>
      break;
    case "layout-2":
      renderLayout = <Layout2 layouts={layouts} setLayoutSetUp={setLayoutSetUp}/>
      break;
    case "layout-3":
      renderLayout = <Layout3 layouts={layouts} setLayoutSetup={setLayoutSetUp}/>
      break;
  }


  const handleSaveLayout = async() => {
    console.log(selectedLayout)
    console.log(layoutSetup)
    await fetch("https://ajtfbqn9d1.execute-api.ap-southeast-1.amazonaws.com/Prod/api/SetupUI", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        organizationUrl: "test.atlassian.net",
        jiraProjectId: "10001",
        layoutSetup: JSON.stringify({
          layout: selectedLayout,
          detail: layoutSetup,
          items: layouts
        })
      })
    })

    message.success("success!")
  }

  const handleDragStart: DragDropContextProps["onBeforeDragStart"] = ({draggableId}) => {
    const selectedItem = fieldItems[parseInt(draggableId)-1];
    setItemDragging(selectedItem)
  }


  return (
    <Row gutter={30}>
      <DragDropContext onDragEnd={handleDragEnd} onDragStart={handleDragStart}>
        <Col>
          {renderLayout}
        </Col>
        <Col span={2}>
          <b>Layout: </b>
          <Select
              defaultValue="layout-1"
              style={{ width: "50%", marginBottom:"10px" }}
              onChange={handleSelectLayoutType}
              options={[
                { value: 'layout-1', label: 'Layout 1' },
                { value: 'layout-2', label: 'Layout 2' },
                { value: 'layout-3', label: 'Layout 3' },
              ]}
            />
          <Button type='primary' onClick={async () => {await handleSaveLayout()}}>Save layout</Button>
        </Col>
        <Col>
          <b>Custom fields: </b>
          <Droppable droppableId={"pool"} key={"pool"}>
                {(provided, snapshot) => (
                    <DroppableRoot
                        ref={provided.innerRef}
                        {...provided.droppableProps}
                        isDraggingOver={snapshot.isDraggingOver}
                    >
                        {
                          fieldItems.map(
                            (field, index) => {
                              return (
                                <Draggable key={field.id} draggableId={field.id} index={index}>
                                  {(provided, snapshot) => (
                                      <div
                                        key={field.id}
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                      >
                                        <FieldCardComponent {...field} />
                                      </div>
                                  )}
                                </Draggable>
                              )
                            }
                          )
                        }
                    </DroppableRoot>
                )}
          </Droppable>
          
        </Col>
      </DragDropContext>
    </Row>
  );  
}
export default App;


