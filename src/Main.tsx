import React from 'react';
import { Tabs } from 'antd';
import type { TabsProps } from 'antd';
import App from './App';
import MainBoard from './MainBoard';

const onChange = (key: string) => {
};

const items: TabsProps['items'] = [
  {
    key: '1',
    label: 'Layout config',
    children: <App />,
  },
  {
    key: '2',
    label: 'Board',
    children: <MainBoard />,
  }
];

const Main: React.FC = () => <Tabs defaultActiveKey="1" items={items} onChange={onChange} />;

export default Main;