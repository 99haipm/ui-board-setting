import { Layout, Typography } from "antd"
import Taskboard from "./components/taskBoard/Taskboard"
import styled from "styled-components"
import { Content, Header } from "antd/es/layout/layout"



const MainBoard: React.FC = () => {
    const StyledLayout = styled(Layout)`
        position: absolute;
        top:0;
        bottom:0;
        left:0;
        right:0;
        height:60vh
    `

    const StyledHeader = styled(Header)`
        display: flex;
        align-items: center;
        background-color: #fff;
    `
    const StyleContent = styled(Content)`
        background-color: white
    `

    return(
        <StyledLayout>
            <StyleContent>
                <Taskboard/>
            </StyleContent>
        </StyledLayout>
    )
}


export default MainBoard;