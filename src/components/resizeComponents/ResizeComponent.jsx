import { ReactNode, useState } from 'react';
import SplitPane, { Pane } from 'split-pane-react';
import 'split-pane-react/esm/themes/default.css'

function ResizeComponent () {
    const [sizes, setSizes] = useState([1000, 'auto']);
    const [sizes1, setSizes1] = useState([200, 'auto']);

    const layoutCSS = {
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    };

    return (
        <div style={{ height: 500 }}>
            <p>Split supports complex layouts</p>
            <SplitPane
                split="vertical"
                sizes={sizes}
                onChange={setSizes}
            >
                <SplitPane sizes={sizes1} onChange={setSizes1} split='horizontal'>
                    <div style={{ ...layoutCSS, background: '#ddd' }}>
                        Top Pane1
                    </div>
                    <div style={{ ...layoutCSS, background: '#d5d7d9' }}>
                        Top Pane2
                    </div>
                </SplitPane>
                <div style={{ ...layoutCSS, background: '#c0c3c6' }}>
                    Bottom Pane1
                </div>
            </SplitPane>
        </div>
    );
};

export default ResizeComponent;