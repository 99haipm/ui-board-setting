import { Card } from "antd";
import { styled } from "styled-components";
import { TaskboardItem, TaskboardItemStatus } from "./TaskboardTypes";
import { Droppable, Draggable } from 'react-beautiful-dnd';
import TaskboardItemCard from "./TaskboardItemCard";


const TaskboardColRoot = styled(Card)`
    user-select: none;
    flex:1;
    margin:0.5rem;
    height:100%;
    display:flex
    flex-direction: column;
    min-width:0;
    > .ant-card-body{
        overflow: hidden;
        height: 100%;
        padding:0;
    };
    background-color:red    
`


interface DroppableRootProps{
    isDraggingOver: boolean;
}

const DroppableRoot = styled.div<DroppableRootProps>`
    height:100%;
    overflow-y: auto;
    background-color: ${({ isDraggingOver }) =>
    isDraggingOver ? "white" : "gray"};
`

export interface TaskboardColProps{
    items: TaskboardItem[];
    status: TaskboardItemStatus;
    onClickDetailCard: (item: TaskboardItem) => any;
}


function TaskboardCol({items, status, onClickDetailCard} : TaskboardColProps){
    return (
        <TaskboardColRoot
            title={`${status}`}
            extra = {
                <p></p>
            }
        >
            <Droppable droppableId={status}>
                {(provided, snapshot) => (
                <DroppableRoot
                    ref={provided.innerRef}
                    {...provided.droppableProps}
                    isDraggingOver={snapshot.isDraggingOver}
                >
                    {
                        items.map((item, index) => {
                            return (
                                <Draggable key={item.id} draggableId={item.id} index={index}>
                                {(provided, snapshot) => (
                                    <div
                                    key={item.id}
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    >
                                    <TaskboardItemCard
                                        item={item}
                                        status={status}
                                        isDrag={snapshot.isDragging}
                                        onClickDetailCard={onClickDetailCard}
                                    />
                                    </div>
                                )}
                                </Draggable>
                            );
                        })
                    }
                    {provided.placeholder}
                </DroppableRoot>
                )}
            </Droppable>

        </TaskboardColRoot>

    )
}


export default TaskboardCol;