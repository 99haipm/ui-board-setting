import { styled } from "styled-components";
import { TaskboardItemStatus, TaskboardItem } from "./TaskboardTypes";
import { Card, Typography } from "antd";
import { useState } from "react";

export interface TaskboardItemCardProps{
    item: TaskboardItem;
    status: TaskboardItemStatus;
    isDrag: boolean;
    onClickDetailCard: (item: TaskboardItem) => any;
}

interface StyleCardProps {
    $isDragging: boolean;
}


const StyleCard = styled(Card)<StyleCardProps>`
    margin: 0.5rem;
    padding: 0.5rem;
    background-color: ${({$isDragging}) => ($isDragging ? '#fafafa' : '#fff')};
`
const TaskboardItemCardTitle = styled(Typography.Title)`
  white-space: pre-wrap;
  // To make ellipsis of the title visible.
  // Without this margin, it can be go behind the "extra" component.
  // So, we give it a little space.
  margin-right: 0.25rem;
`;

function TaskboardItemCard({item,status,isDrag, onClickDetailCard} : TaskboardItemCardProps){
    return(
        <StyleCard 
            $isDragging={isDrag} 
            size="small" 
            title = {
                <span>
                    <TaskboardItemCardTitle level={5} ellipsis={{ rows: 2 }}>
                    {item.title}
                    </TaskboardItemCardTitle>
                </span>
            }
            extra={
                <div>
                    <p>...</p>
                </div>
            }
            onClick={() => {onClickDetailCard(item)}}
        >
            <Typography.Paragraph type="secondary" ellipsis={{ rows: 2 }}>
            {item.description}
            </Typography.Paragraph>
        </StyleCard>
    )
}



export default TaskboardItemCard;