import { styled } from "styled-components";
import { TaskboardItem, TaskboardItemStatus } from "./TaskboardTypes";
import { useEffect, useState } from "react";
import { DragDropContext, DragDropContextProps } from "react-beautiful-dnd";
import TaskboardCol from "./TaskboardCol";
import { Modal } from "antd";
import { ModalLayout1, ModalLayout2, ModalLayout3 } from "../shared/buildLayout";



const TaskboardRoot = styled.div`
    min-height:0;
    height:100%;
    min-width: 800px;
    max-width: 1400px;
    margin: auto;
`


const TaskboardContent = styled.div`
    height:100%;
    padding:0.5rem;
    display:flex;
    justify-content: space-around;
`


const defaultItems:TaskboardData = {
    [TaskboardItemStatus.TO_TO] : [
        {
            id: "1",
            title:"Task 1",
            description: "description task 1"
        },
        {
            id: "2",
            title:"Task 2",
            description: "description task 2"
        }
    ],
    [TaskboardItemStatus.IN_PROGRESS] : [],
    [TaskboardItemStatus.DONE] : []
}

type TaskboardData = Record<TaskboardItemStatus, TaskboardItem[]>

interface LayoutDetail{
    Vertical: number[];
    Horizontal: number[];
}
interface LayoutConfig{
    layout: string;
    detail: LayoutDetail;
}
 
function Taskboard(){
    const [layoutConfig, setLayoutConfig] = useState<LayoutConfig>();
    const [itemByStatus, setItemByStatus] = useState<TaskboardData>(
        defaultItems
    );

    useEffect(
        () => {
            const getLayoutConfig = async () : Promise<LayoutConfig> => {
                const organization = "test.atlassian.net";
                const jiraProjectId = "10001";
                const request = await fetch(`https://ajtfbqn9d1.execute-api.ap-southeast-1.amazonaws.com/Prod/api/SetupUI/${organization}/${jiraProjectId}`, {
                    headers: {
                        Accept: "application/json"
                    },
                    method: "GET"
                });
                const response: LayoutConfig = JSON.parse((await request.json())["layoutSetup"]);
                console.log(response)
                return response;
            }

            const initUseEffect = async () => {
                const layoutConfigResponse = await getLayoutConfig();
                setLayoutConfig(layoutConfigResponse);
            }
            initUseEffect();
        }, []
    )
    const [openModal, setOpenModal] = useState<boolean>();
    const handleDragEnd: DragDropContextProps["onDragEnd"] = ({
        source, destination
    }) => {
        setItemByStatus(prevState => {
            const cloneState: TaskboardData = JSON.parse(JSON.stringify(prevState))
            const keyFrom: string = source.droppableId.toString()
            const ketTo: string| undefined = destination?.droppableId.toString()
            let itemSwitchStatus = undefined
            switch(keyFrom){
                case TaskboardItemStatus.TO_TO:
                    itemSwitchStatus = cloneState["To Do"][source.index]
                    cloneState["To Do"].splice(source.index,1)
                    break;
                case TaskboardItemStatus.IN_PROGRESS:
                    itemSwitchStatus = cloneState["In Progress"][source.index]
                    cloneState["In Progress"].splice(source.index,1)
                    break;
                case TaskboardItemStatus.DONE:
                    itemSwitchStatus = cloneState["Done"][source.index]
                    cloneState["Done"].splice(source.index,1)
                    break;
            }
            if(ketTo && itemSwitchStatus){
                switch(ketTo){
                    case TaskboardItemStatus.TO_TO:
                        cloneState["To Do"].push(itemSwitchStatus)
                        break;
                    case TaskboardItemStatus.IN_PROGRESS:
                        cloneState["In Progress"].push(itemSwitchStatus)
                        break;
                    case TaskboardItemStatus.DONE:
                        cloneState["Done"].push(itemSwitchStatus)
                        break;
                }
            }
            
            return cloneState
        })
    }

    const handleOnClickDetailCard =  (item: TaskboardItem) => {
        setOpenModal(true)
    };
    let modelLayout: React.ReactNode = <></>

    switch(layoutConfig?.layout){
        case "layout-1":
            modelLayout = ModalLayout1(layoutConfig.detail.Vertical, layoutConfig.detail.Horizontal)
            break;
        case "layout-2":
            modelLayout = ModalLayout2(layoutConfig.detail.Vertical)
            break;
        case "layout-3":
            modelLayout = ModalLayout3(layoutConfig.detail.Vertical, layoutConfig.detail.Horizontal)
            break;
    }
    return (
        <>
            <DragDropContext onDragEnd={handleDragEnd}>
                <TaskboardRoot>
                    <TaskboardContent>
                        {
                            Object.values(TaskboardItemStatus).map((status) => {
                                return (<TaskboardCol
                                    items={itemByStatus[status]}
                                    status={status}
                                    key={status}
                                    onClickDetailCard={handleOnClickDetailCard}
                                />)
                            })
                        }
                    </TaskboardContent>
                </TaskboardRoot>
            </DragDropContext>
            <Modal
                centered
                open={openModal}
                onCancel={() => setOpenModal(false)}
                footer={null}
                bodyStyle={
                    {height:"60vh"}
                }
                width={"52vw"}
            >
                {modelLayout}
            </Modal>
        </>
    )
}


export default Taskboard;