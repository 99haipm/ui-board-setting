import react from "react"
import SplitPane from "split-pane-react/esm/SplitPane"

export const ModalLayout1 = (sizeVertical, sizeHorizontal) => {
    const layoutCSS = {
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }; 
    return (
        <div style={{ height: "60vh", width: "50vw" }}>
            <SplitPane
                split="vertical"
                sizes={sizeVertical}
                onChange={() =>{}}
            >
                <SplitPane sizes={sizeHorizontal} onChange={() => {}} split='horizontal'>
                    <div style={{...layoutCSS, borderRight:"1px solid #a4aba6"}}>

                    </div>
                    <div style={{...layoutCSS, borderTop:"1px solid #a4aba6", borderRight:"1px solid #a4aba6"}}>

                    </div>
                    
                </SplitPane>
                <div style={{...layoutCSS}}>

                </div>
            </SplitPane>
        </div>
    )
}

export const ModalLayout2= (sizeVertical) => {
    const layoutCSS = {
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }; 
    return (
        <div style={{ height: "60vh", width: "50vw" }}>
            <SplitPane
                split="vertical"
                sizes={sizeVertical}
                onChange={() =>{}}
            >
                <div style={{...layoutCSS, borderRight:"1px solid #a4aba6"}}>

                </div>
                <div style={{...layoutCSS, borderRight:"1px solid #a4aba6"}}>

                </div>
                <div style={{...layoutCSS}}>

                </div>
            </SplitPane>
        </div>
    )
}

export const ModalLayout3= (sizeVertical, sizeHorizontal) => {
    const layoutCSS = {
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }; 
    return (
        <div style={{ height: "60vh", width: "50vw" }}>
            <SplitPane
                split="vertical"
                sizes={sizeVertical}
                onChange={() =>{}}
            >
                <SplitPane
                    split="horizontal"
                    sizes={sizeHorizontal}
                    onChange={() =>{}}
                >
                    <div style={{...layoutCSS, borderRight:"1px solid #a4aba6"}}>

                    </div>
                    <div style={{...layoutCSS, borderTop:"1px solid #a4aba6", borderRight:"1px solid #a4aba6"}}>

                    </div>
                </SplitPane>
                
                <SplitPane
                    split="horizontal"
                    sizes={sizeHorizontal}
                    onChange={() =>{}}
                >
                    <div style={{...layoutCSS}}>

                    </div>
                    <div style={{...layoutCSS,  borderTop:"1px solid #a4aba6"}}>

                    </div>
                </SplitPane>
            </SplitPane>
        </div>
    )
}