import { Draggable, Droppable } from "react-beautiful-dnd";
import { styled } from "styled-components";
import FieldCardComponent from "../fieldComponents/FieldComponent";
import SplitPane from "split-pane-react/esm/SplitPane";
import { useState } from "react";
import 'split-pane-react/esm/themes/default.css'

const Layout1 = ({layouts, setLayoutSetup, itemDragging}) => {
    console.log("render layout 1")
    const [sizeVertical, setSizeVertical] = useState([1000, 'auto'])
    const [sizeHorizontal, setSizeHorizontal] = useState([200, 'auto'])
    const layoutCSS = {
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }; 
    const DroppableRoot = styled.div`
        height:100%;
        padding:10px;
        overflow-y: auto;
        background-color: ${({ isDraggingOver, bgColor, itemDragging, index }) =>{
            if(itemDragging && itemDragging.layoutPermission.layout1 && index === 0){
                return "red";
            }
            if(itemDragging && itemDragging.layoutPermission.layout2 && index === 1){
                return "yellow";
            }
            if(itemDragging && itemDragging.layoutPermission.layout3 && index === 2){
                return "green";
            }
            return bgColor
        }};
    `
   

    const handleSizeVerticalChange = (value) => {
        setSizeVertical(value)
        setLayoutSetup(prevState => {
            const cloneState = JSON.parse(JSON.stringify(prevState))
            cloneState["Vertical"] = value
            cloneState["Horizontal"] = sizeHorizontal
            return cloneState
        })
    }

    const handleSizeHorizontalChange = (value) => {
        setSizeHorizontal(value)
        setLayoutSetup(prevState => {
            const cloneState = JSON.parse(JSON.stringify(prevState))
            cloneState["Horizontal"] = value
            cloneState["Vertical"] = sizeVertical
            return cloneState
        })
    }
    const buildLayout = (index) => {
        if(!itemDragging){
            return (
                <Droppable style={{ ...layoutCSS }} droppableId={layouts[index].id} key={layouts[index].id}>
                    {(provided, snapshot) => (
                        <DroppableRoot
                            ref={provided.innerRef}
                            {...provided.droppableProps}
                            isDraggingOver={snapshot.isDraggingOver}
                            bgColor={index === 0 ? "#c0c3c6" : index === 1 ?  "#d5d7d9" : "#ddd"}
                            itemDragging={itemDragging}
                            index={index}
                        >
                            {
                                buildSectionDroppable(layouts[index].items, layouts[index].id)
                            }
                        </DroppableRoot>
                    )}
                </Droppable>
            )
        }
        if((itemDragging.layoutPermission.layout1 && index === 0) || (itemDragging.layoutPermission.layout2 && index === 1) || (itemDragging.layoutPermission.layout3 && index === 2)){
            return (
                <Droppable style={{ ...layoutCSS }} droppableId={layouts[index].id} key={layouts[index].id}>
                    {(provided, snapshot) => (
                        <DroppableRoot
                            ref={provided.innerRef}
                            {...provided.droppableProps}
                            isDraggingOver={snapshot.isDraggingOver}
                            bgColor={index === 0 ? "#c0c3c6" : index === 1 ?  "#d5d7d9" : "#ddd"}
                            itemDragging={itemDragging}
                            index={index}
                        >
                            {
                                buildSectionDroppable(layouts[index].items, layouts[index].id)
                            }
                        </DroppableRoot>
                    )}
                </Droppable>
            )
        }
        return (
            <div>
                <p>No permission</p>
            </div>
        )
    }
    const layout1 = buildLayout(0)
    const layout2 = buildLayout(1)
    const layout3 = buildLayout(2)
    return (
        <div style={{ height: "60vh", width: "50vw" }}>
                <SplitPane
                split="vertical"
                sizes={sizeVertical}
                onChange={handleSizeVerticalChange}
            >
                <SplitPane sizes={sizeHorizontal} onChange={handleSizeHorizontalChange} split='horizontal'>
                    {
                        layout1
                    }
                    {
                        layout2
                    }
                </SplitPane>
                {
                    layout3
                }
            </SplitPane>
        </div>
    )
}

const buildSectionDroppable = (items, key) => {
    return items.map(
        (field,index) => {
            return (
                <Draggable key={field.id} draggableId={field.id} index={index} disableInteractiveElementBlocking={true}>
                    {(provided, snapshot) => (
                        <div
                            key={key}
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            
                        >
                            <FieldCardComponent {...field} />
                        </div>
                    )}
                </Draggable>
            )
        }
    )
}


export default Layout1;