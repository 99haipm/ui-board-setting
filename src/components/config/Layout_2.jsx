import { Draggable, Droppable } from "react-beautiful-dnd";
import { styled } from "styled-components";
import FieldCardComponent, { FieldComponentProps } from "../fieldComponents/FieldComponent";
import SplitPane from "split-pane-react/esm/SplitPane";
import { useRef, useState } from "react";
import 'split-pane-react/esm/themes/default.css'



const Layout2 = ({layouts, setLayoutSetUp}) => {
    const windowSize = useRef([window.innerWidth, window.innerHeight]);
    const viewWidth = windowSize.current[0]
    const currentViewHeight = (62*windowSize.current[1])/100
    const [sizeVertical, setSizeVertical] = useState([500, 'auto'])
    const layoutCSS = {
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    };
    const DroppableRoot = styled.div`
        height:100%;
        padding:10px;
        overflow-y: auto;
        background-color: ${({ isDraggingOver, bgColor }) =>
        isDraggingOver ? "rgba(9,30,66,.25)" : bgColor};
    `
    const handleSizeVerticalChange = (value) => {
        setSizeVertical(value)
        setLayoutSetUp(prevState => {
            const cloneState = JSON.parse(JSON.stringify(prevState))
            cloneState["Vertical"] = value
            return cloneState
        })
    }
    return (
        <div style={{ height: "60vh", width:"50vw" }}>
                <SplitPane
                split="vertical"
                sizes={sizeVertical}
                onChange={handleSizeVerticalChange}
            >
                 <Droppable style={{ ...layoutCSS }} droppableId={layouts[0].id} key={layouts[0].id}>
                        {(provided, snapshot) => (
                            <DroppableRoot
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                                isDraggingOver={snapshot.isDraggingOver}
                                bgColor="#c0c3c6"
                            >
                                {
                                    layouts[0].items.map(
                                        (field,index) => {
                                            return (
                                                <Draggable key={field.id} draggableId={field.id} index={index}>
                                                    {(provided, snapshot) => (
                                                        <div
                                                            key={layouts[0].id}
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                        >
                                                            <FieldCardComponent {...field} />
                                                        </div>
                                                    )}
                                                </Draggable>
                                            )
                                        }
                                    )
                                }
                            </DroppableRoot>
                        )}
                    </Droppable>
                    <Droppable style={{ ...layoutCSS, background: '#ddd' }} droppableId={layouts[1].id} key={layouts[1].id}>
                        {(provided, snapshot) => (
                            <DroppableRoot
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                                isDraggingOver={snapshot.isDraggingOver}
                                bgColor="#d5d7d9"
                            >
                                {
                                    layouts[1].items.map(
                                        (field,index) => {
                                            return (
                                                <Draggable key={field.id} draggableId={field.id} index={index}>
                                                    {(provided, snapshot) => (
                                                        <div
                                                            key={layouts[1].id}
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                        >
                                                            <FieldCardComponent {...field} />
                                                        </div>
                                                    )}
                                                </Draggable>
                                            )
                                        }
                                    )
                                }
                            </DroppableRoot>
                        )}
                    </Droppable>
                <Droppable style={{ ...layoutCSS }} droppableId={layouts[2].id} key={layouts[2].id}>
                        {(provided, snapshot) => (
                            <DroppableRoot
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                                isDraggingOver={snapshot.isDraggingOver}
                                bgColor="#ddd"
                            >
                                {
                                    layouts[2].items.map(
                                        (field,index) => {
                                            return (
                                                <Draggable key={field.id} draggableId={field.id} index={index}>
                                                    {(provided, snapshot) => (
                                                        <div
                                                            key={layouts[2].id}
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                        >
                                                            <FieldCardComponent {...field} />
                                                        </div>
                                                    )}
                                                </Draggable>
                                            )
                                        }
                                    )
                                }
                            </DroppableRoot>
                        )}
                    </Droppable>
            </SplitPane>
        </div>
    )
}


export default Layout2;