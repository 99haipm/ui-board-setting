import { Draggable, Droppable } from "react-beautiful-dnd";
import { styled } from "styled-components";
import FieldCardComponent, { FieldComponentProps } from "../fieldComponents/FieldComponent";
import SplitPane from "split-pane-react/esm/SplitPane";
import { useRef, useState } from "react";
import 'split-pane-react/esm/themes/default.css'

const Layout3 = ({layouts, setLayoutSetup}) => {
    const [sizeVertical, setSizeVertical] = useState([1000, 'auto'])
    const [sizeHorizontal, setSizeHorizontal] = useState([300, 'auto'])
    const layoutCSS = {
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    };
    const DroppableRoot = styled.div`
        height:100%;
        padding:10px;
        overflow-y: auto;
        background-color: ${({ isDraggingOver, bgColor }) =>
        isDraggingOver ? "rgba(9,30,66,.25)" : bgColor};
    `
    const handleSizeVerticalChange = (value) => {
        setSizeVertical(value)
        setLayoutSetup(prevState => {
            const cloneState = JSON.parse(JSON.stringify(prevState))
            cloneState["Vertical"] = value
            cloneState["Horizontal"] = sizeHorizontal
            return cloneState
        })
    }

    const handleSizeHorizontalChange = (value) => {
        setSizeHorizontal(value)
        setLayoutSetup(prevState => {
            const cloneState = JSON.parse(JSON.stringify(prevState))
            cloneState["Horizontal"] = value
            cloneState["Vertical"] = sizeVertical
            return cloneState
        })
    }
    return (
        <div style={{ height: "60vh", width: "50vw" }}>
                <SplitPane
                split="vertical"
                sizes={sizeVertical}
                onChange={handleSizeVerticalChange}
            >
                <SplitPane sizes={sizeHorizontal} onChange={handleSizeHorizontalChange} split='horizontal'>
                    <Droppable style={{ ...layoutCSS }} droppableId={layouts[0].id} key={layouts[0].id}>
                        {(provided, snapshot) => (
                            <DroppableRoot
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                                isDraggingOver={snapshot.isDraggingOver}
                                bgColor="#c0c3c6"
                            >
                                {
                                    layouts[0].items.map(
                                        (field,index) => {
                                            return (
                                                <Draggable key={field.id} draggableId={field.id} index={index}>
                                                    {(provided, snapshot) => (
                                                        <div
                                                            key={layouts[0].id}
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                        >
                                                            <FieldCardComponent {...field} />
                                                        </div>
                                                    )}
                                                </Draggable>
                                            )
                                        }
                                    )
                                }
                            </DroppableRoot>
                        )}
                    </Droppable>
                    <Droppable style={{ ...layoutCSS, background: '#ddd' }} droppableId={layouts[1].id} key={layouts[1].id}>
                        {(provided, snapshot) => (
                            <DroppableRoot
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                                isDraggingOver={snapshot.isDraggingOver}
                                bgColor="#d5d7d9"
                            >
                                {
                                    layouts[1].items.map(
                                        (field,index) => {
                                            return (
                                                <Draggable key={field.id} draggableId={field.id} index={index}>
                                                    {(provided, snapshot) => (
                                                        <div
                                                            key={layouts[1].id}
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                        >
                                                            <FieldCardComponent {...field} />
                                                        </div>
                                                    )}
                                                </Draggable>
                                            )
                                        }
                                    )
                                }
                            </DroppableRoot>
                        )}
                    </Droppable>
                </SplitPane>
                <SplitPane sizes={sizeHorizontal} onChange={handleSizeHorizontalChange} split='horizontal'>
                    <Droppable style={{ ...layoutCSS }} droppableId={layouts[2].id} key={layouts[2].id}>
                        {(provided, snapshot) => (
                            <DroppableRoot
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                                isDraggingOver={snapshot.isDraggingOver}
                                bgColor="#ddd"
                            >
                                {
                                    layouts[2].items.map(
                                        (field,index) => {
                                            return (
                                                <Draggable key={field.id} draggableId={field.id} index={index}>
                                                    {(provided, snapshot) => (
                                                        <div
                                                            key={layouts[2].id}
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                        >
                                                            <FieldCardComponent {...field} />
                                                        </div>
                                                    )}
                                                </Draggable>
                                            )
                                        }
                                    )
                                }
                            </DroppableRoot>
                        )}
                    </Droppable>
                    <Droppable style={{ ...layoutCSS }} droppableId={layouts[3].id} key={layouts[3].id}>
                        {(provided, snapshot) => (
                            <DroppableRoot
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                                isDraggingOver={snapshot.isDraggingOver}
                                bgColor="#c0c3c6"
                            >
                                {
                                    layouts[3].items.map(
                                        (field,index) => {
                                            return (
                                                <Draggable key={field.id} draggableId={field.id} index={index}>
                                                    {(provided, snapshot) => (
                                                        <div
                                                            key={layouts[3].id}
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                        >
                                                            <FieldCardComponent {...field} />
                                                        </div>
                                                    )}
                                                </Draggable>
                                            )
                                        }
                                    )
                                }
                            </DroppableRoot>
                        )}
                    </Droppable>
                </SplitPane>
                
            </SplitPane>
        </div>
    )
}


export default Layout3;