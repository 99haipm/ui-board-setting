
import { FieldComponentProps } from "../fieldComponents/FieldComponent";



export interface LayoutSetting {
    id: string;
    name: string;
    items: FieldComponentProps[]
}