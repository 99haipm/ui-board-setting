import { Card, Typography } from "antd";
import { styled } from "styled-components";




export interface FieldComponentProps{
    id: string;
    name: string;
    description: string;
    type?: string;
    layoutPermission? :any
}



const StyleFieldCard = styled.div`
    width: 50%;
    height:3vh;
    background-color: white;
    box-shadow: 1px 1px 2px 1px rgba(9,30,66,.25);
    border-radius: 2px;
    padding: 5px 5px;
    margin-bottom: 10px;
    font-size: 100%
` 

const FieldCardComponent: React.FC<FieldComponentProps> = (props: FieldComponentProps) => {
    return (
        <StyleFieldCard>
                <p>{props.name}</p>
        </StyleFieldCard>
    );
}

export default FieldCardComponent;


